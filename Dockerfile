FROM python:3.6-alpine

COPY * /app/
COPY app/* /app/app/

WORKDIR /app

RUN  pip install -qr requirements.txt \
    && pip install gunicorn

ENTRYPOINT [ "gunicorn", "-w", "2", "-b", "0.0.0.0:5000", "run:app"  ]