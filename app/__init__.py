#!/usr/bin/env python
# -*- coding:utf-8 -*-
from flask import Flask


def create_app(config_name):
    app = Flask(__name__)

    from app.view import main_blueprint
    app.register_blueprint(main_blueprint)

    return app
