#!/usr/bin/env python
# -*- coding:utf-8 -*-
from flask import app, Blueprint

main_blueprint = Blueprint('main',__name__)

@main_blueprint.route('/')
def index():
    return 'Index'


@main_blueprint.route('/hello/')
def hello_world_default():
    return "Hello World !"


@main_blueprint.route('/hello/<int:name>')
def hello_world(name):
    return "Hello %d !" % name
